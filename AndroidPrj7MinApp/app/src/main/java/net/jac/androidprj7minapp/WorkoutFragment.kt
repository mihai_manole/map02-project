package net.jac.androidprj7minapp


import android.app.AlertDialog
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.text.InputType
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.fragment_workout.*
import kotlinx.android.synthetic.main.fragment_workout.view.*
import net.jac.androidprj7minapp.R.id.spUsers
import org.w3c.dom.Text
import java.util.ArrayList


class WorkoutFragment : Fragment() {

    val TAG = "WorkoutFragment"
//    internal val context: Context? = getContext()
//    internal lateinit var spUsers: Spinner
    internal lateinit var sharedPreferences: SharedPreferences

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_workout,container,false).apply {
            btStart.setOnClickListener{
//                Toast.makeText(container?.context,"Start",Toast.LENGTH_LONG).show()
                startActivity(Intent(container?.context,ExercisesActivity::class.java))
            }

            btInstructions.setOnClickListener {
                (activity as MainActivity).tabs.getTabAt(1)?.select()
            }

            tvAddUser.setOnClickListener { onClickAddUser(it) }

            // FIXME
//            spUsers = findViewById<View>(R.id.spUsers) as Spinner

            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(container?.context)
            Globals.userId = sharedPreferences.getInt(Globals.PREF_ID, 0)
            Globals.userName = sharedPreferences.getString(Globals.PREF_NAME, "")
            showUsers(spUsers)

            spUsers.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

                    val newUserName = spUsers.selectedItem as String
                    val newUserId = DatabaseHelper.getUserIdByName(context, newUserName)
                    if (newUserId <= 0) {
                        // Failed to fetch the data from the database
                        Toast.makeText(context, resources.getString(R.string.failed_to_fetch), Toast.LENGTH_LONG).show()
                    } else {
                        val showToast = Globals.userId != newUserId;
                        Globals.userName = newUserName
                        Globals.userId = newUserId
                        val editor = sharedPreferences.edit()
                        editor.putInt(Globals.PREF_ID, newUserId)
                        editor.putString(Globals.PREF_NAME, newUserName)
                        editor.apply()

                        // New user %s is selected
                        var s = resources.getString(R.string.new_user_selected)
                        s = String.format(s, newUserName)
                        if (showToast)
                            Toast.makeText(context, s, Toast.LENGTH_LONG).show()

                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {

                    // sometimes you need nothing here
                }
            }

        }
    }

    override fun onStart() {
        super.onStart()

        showUsers(spUsers)
    }

    fun onClickAddUser(view: View) {

        val etUserName = EditText(context)
        etUserName.inputType = InputType.TYPE_CLASS_TEXT
        etUserName.setText("")

        val dialog = AlertDialog.Builder(context)
            .setView(etUserName)
            .setTitle(R.string.add_new_user)
            .setPositiveButton(R.string.add, null) //Set to null. We override the onclick
            .setNegativeButton(R.string.cancel, null)
            .create()

        dialog.setOnShowListener { _ ->
            val btnOK = (dialog as AlertDialog).getButton(AlertDialog.BUTTON_POSITIVE)
            btnOK.setOnClickListener {
                val newName = etUserName.text.toString()

                if (newName.isNotEmpty()) {
                    //Add the new user to users table
                    val newUserId: Int = DatabaseHelper.addUser(context, newName)
                    var s: String
                    when (newUserId) {
                        -1 -> {
                            Log.d(TAG, "User $newName already exist")
                            s = resources.getString(R.string.user_already_exists)
                            s = String.format(s, newName)
                            Toast.makeText(context, s, Toast.LENGTH_LONG).show()
                        }
                        0 -> {
                            s = resources.getString(R.string.couldnt_add)
                            s = String.format(s, newName)
                            Toast.makeText(context, s, Toast.LENGTH_LONG).show();
                            Log.d(TAG, "Couldn't add and initialize user $newName")
                        }
                        else -> {
                            Globals.userId = newUserId
                            Globals.userName = newName
                            Log.d(TAG, "New user \"$newName\" added")
                            s = resources.getString(R.string.new_user_added)
                            s = String.format(s, newName)
                            Toast.makeText(context, s, Toast.LENGTH_LONG).show();
                            showUsers(spUsers)

                            dialog.dismiss()
                        }
                    }

                }
            }

            val btnCancel = dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
            btnCancel.setOnClickListener {
                //Close the dialog
                dialog.cancel()
            }
        }
        dialog.show()

    }

    private fun showUsers(spUsers: Spinner) {
        val context=context ?: return
        val userNames = ArrayList<String>()
        DatabaseHelper.getUserNames(context,userNames)

        // If users list is empty - add the "Guest User"
        if (userNames.isEmpty()) {
            DatabaseHelper.addUser(context, "Guest User")
            DatabaseHelper.getUserNames(context,userNames)
        }

        val listAdapter = ArrayAdapter(
            context, android.R.layout.simple_list_item_1, userNames
        )
        spUsers.adapter = listAdapter

        // Move spinner position to current value
        var selection = userNames.indexOf(Globals.userName)
        if (selection == -1) selection = 0

        spUsers.setSelection(selection)

    }


}
