package net.jac.androidprj7minapp;

public class Globals {

    public static int userId = 0;
    public static String userName;
    public static final String PREF_ID = "id";
    public static final String PREF_NAME = "name";

    static WorkoutStatus workoutStatus;
    static boolean wasRunning;

}
