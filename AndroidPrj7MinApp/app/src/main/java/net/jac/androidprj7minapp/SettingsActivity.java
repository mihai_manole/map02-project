package net.jac.androidprj7minapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.*;
import org.w3c.dom.Text;

import java.lang.reflect.Field;
import java.util.ArrayList;

public class SettingsActivity extends AppCompatActivity  {

    public static final String TAG = "SettingsActivity";
    final Context context = this;

    Settings.SettingsProperty settingsPropertyGlobal;

    Spinner spNumber;
    TextView tvRepeatCircuits, tvExercisePeriod, tvRestSet, tvCountdownTime;
    Switch swSound, swTTSVoice;
    RelativeLayout rlRepeatCircuits, rlExercisePeriod, rlRestSet, rlCountdownTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        tvRepeatCircuits = (TextView) findViewById(R.id.tvRepeatCircuits);
        tvExercisePeriod = (TextView) findViewById(R.id.tvExercisePeriod);
        tvRestSet = (TextView) findViewById(R.id.tvRestSet);
        tvCountdownTime = (TextView) findViewById(R.id.tvCountDownTime);
        swSound = (Switch) findViewById(R.id.swSound);
        swTTSVoice = (Switch) findViewById(R.id.swTTSVoice);

        rlRepeatCircuits = (RelativeLayout) findViewById(R.id.rlRepeatCircuits);
        rlExercisePeriod = (RelativeLayout) findViewById(R.id.rlExercisePeriod);
        rlRestSet = (RelativeLayout) findViewById(R.id.rlRestSet);
        rlCountdownTime = (RelativeLayout) findViewById(R.id.rlCountdownTime);


        // FIXME: This code will go to the MainActivity ( if needed outside of SettingsActivity )
        //Settings.settings = new Settings();

         // Initialize settings
        loadSettings();
        initializeViews();

    }

    // FIXME: This method will be duplicated in MainActivity ( if needed outside of SettingsActivity )
    public void loadSettings() {
        Settings.settings = DatabaseHelper.loadSettings(this, Globals.userId);
//        String msg = "" + Settings.settings.getUserId() + "\n" +
//                Settings.settings.getDateOfBirth() + "\n" +
//                Settings.settings.getHeight() + "\n" +
//                Settings.settings.getRepeatCircuits() + "\n" +
//                Settings.settings.getExercisePeriod() + "\n" +
//                Settings.settings.getRest() + "\n" +
//                Settings.settings.getCountdown() + "\n" +
//                Settings.settings.isSound() + "\n" +
//                Settings.settings.isVoice() + "\n" +
//                Settings.settings.isMetric();
//
//        Toast toast = Toast.makeText(this, msg, Toast.LENGTH_LONG);
//        toast.show();

    }

    public void initializeViews() {
        String times = " " + getResources().getString(R.string.times);
        String secs = " " + getResources().getString(R.string.secs);
        tvRepeatCircuits.setText(String.valueOf(Settings.settings.getRepeatCircuits()) + times);
        tvExercisePeriod.setText(String.valueOf(Settings.settings.getExercisePeriod()) + secs);
        tvRestSet.setText(String.valueOf(Settings.settings.getRest()) + secs);
        tvCountdownTime.setText(String.valueOf(Settings.settings.getCountdown()) + secs);
        swSound.setChecked(Settings.settings.isSound());
        swTTSVoice.setChecked(Settings.settings.isVoice());

    }

    public void onClickSetting(View view) {
        if (view == rlRepeatCircuits) {
            showSpinnerDialog(Settings.SettingsProperty.REPEAT_CIRCUITS);
        } else if (view == rlExercisePeriod) {
            showSpinnerDialog(Settings.SettingsProperty.EXERCISE_PERIOD);
        } else if (view == rlRestSet) {
            showSpinnerDialog(Settings.SettingsProperty.REST);
        } else if (view == rlCountdownTime) {
            showSpinnerDialog(Settings.SettingsProperty.COUNTDOWN);
        }
    }

    public void showSpinnerDialog(Settings.SettingsProperty settingsProperty) {

        // Needs to use class field to pass it to onClickListener
        settingsPropertyGlobal = settingsProperty;

        // Initialize the display variables
        ArrayList<Integer> list = new ArrayList<>();
        String displayText = "";
        int oldValue = 0;

        switch (settingsProperty) {

            case REPEAT_CIRCUITS:
                // Repeat Circuits
                displayText = getResources().getString(R.string.set) + " " + getResources().getString(R.string.times) + " (1 - 6)";
                // 1 - 6 times only
                for (int i = 1; i <= 6; i++) {
                    list.add(i);
                }
                oldValue = Settings.settings.getRepeatCircuits();
                break;

            case EXERCISE_PERIOD:
                // Each exercise period
                displayText = getResources().getString(R.string.set_duration)  + " (10 - 60)";
                // 1 - 6 times only
                for (int i = 10; i <= 60; i+=10) {
                    list.add(i);
                }
                oldValue = Settings.settings.getExercisePeriod();
                break;

            case REST:
                // Rest set
                displayText = getResources().getString(R.string.set_duration)  + " (5 - 30)";
                // 1 - 6 times only
                for (int i = 5; i <= 30; i+=5) {
                    list.add(i);
                }
                oldValue = Settings.settings.getRest();
                break;

            case COUNTDOWN:
                // Countdown time
                displayText = getResources().getString(R.string.set_duration)  + " (10 - 15)";
                // 1 - 6 times only
                list.add(10);
                list.add(15);
                oldValue = Settings.settings.getCountdown();
                break;

            default:
                Log.d(TAG, "showSpinnerDialog -> Invalid argument: " + settingsProperty.toString());
        }

        // create a Dialog component
        final Dialog dialog = new Dialog(context);

        //Tell the Dialog to use the pick_number_dialog.xml as it's layout description
        dialog.setContentView(R.layout.pick_number_dialog);
        dialog.setTitle("Setting Selection");

        TextView tvDescription = (TextView) dialog.findViewById(R.id.tvDescription);

        tvDescription.setText(displayText);

        spNumber = (Spinner) dialog.findViewById(R.id.spNumber);

        // Find the position of the current value, set to first element if not found.
        int position = list.indexOf(oldValue);
        Log.d(TAG, "showSpinnerDialog: " + oldValue + ", " + position);
        if (position == -1) position = 0;

        // Use Reflection to limit the dropdown list size
        try {
            Field popup = Spinner.class.getDeclaredField("mPopup");
            popup.setAccessible(true);

            // Get private mPopup member variable and try cast to ListPopupWindow
            android.widget.ListPopupWindow popupWindow = (android.widget.ListPopupWindow) popup.get(spNumber);

            // Set popupWindow height to 150dip, but convert to px, because "setHeight" takes pixels as an argument.
            float dip = 150f;
            Resources r = getResources();
            float px = TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    dip,
                    r.getDisplayMetrics()
            );
            popupWindow.setHeight((int)px);
        } catch (NoClassDefFoundError | ClassCastException | NoSuchFieldException | IllegalAccessException e) {
            // silently fail...
        }

        ArrayAdapter<Integer> listAdapter = new ArrayAdapter<>(
                context, android.R.layout.simple_list_item_1, list);
        spNumber.setAdapter(listAdapter);
        // Move spinner position to current value
        spNumber.setSelection(position);


        Button btnCancel = (Button) dialog.findViewById(R.id.btnCancel);
        Button btnSet = (Button) dialog.findViewById(R.id.btnSet);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int newValue = Integer.valueOf(String.valueOf(spNumber.getSelectedItem()));
                int newValue = (int)spNumber.getSelectedItem();

                 DatabaseHelper.updateSetting(context, settingsPropertyGlobal, newValue);

                // Reload settings to confirm the change in DB was successful
                loadSettings();
                initializeViews();

                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void onClickSound(View view) {
        swSound.setChecked(!swSound.isChecked());
        int newValue = 0;
        if (swSound.isChecked()) newValue = 1;

        DatabaseHelper.updateSetting(context, Settings.SettingsProperty.SOUND, newValue);

        // Reload settings to confirm the change in DB was successful
        loadSettings();
        initializeViews();

    }

    public void onClickTTSVoice(View view) {
        swTTSVoice.setChecked(!swTTSVoice.isChecked());
        int newValue = 0;
        if (swSound.isChecked()) newValue = 1;

        DatabaseHelper.updateSetting(context, Settings.SettingsProperty.VOICE, newValue);

        // Reload settings to confirm the change in DB was successful
        loadSettings();
        initializeViews();

    }

}
