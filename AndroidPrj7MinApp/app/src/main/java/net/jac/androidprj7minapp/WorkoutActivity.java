package net.jac.androidprj7minapp;

import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class WorkoutActivity extends AppCompatActivity {

    public static final String TAG = "WorkoutActivity";
    ArrayList<Exercise> exercisesList;
    enum WorkoutAction { NONE, START, TAP, RUN, PAUSE, PREVIOUS, NEXT, STOP }
    enum TimerType { NONE, COUNTDOWN, EXERCISE, REST }

    TextView tvTime, tvRound, tvCountType, tvDescription;
    ImageView ivExercise;
    SeekBar sbTimeProgress;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workout);

        tvTime = (TextView) findViewById(R.id.tvTime);
        tvRound = (TextView) findViewById(R.id.tvRound);
        tvCountType = (TextView) findViewById(R.id.tvCountType);
        tvDescription = (TextView) findViewById(R.id.tvDescription);
        ivExercise = (ImageView) findViewById(R.id.ivExercise);
        sbTimeProgress = (SeekBar) findViewById(R.id.sbTimeProgress);
        // Change SeekBar so it doesn't respond to user clicks
        sbTimeProgress.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        sbTimeProgress.setMax(100);

        // Initialize settings
        loadSettings();
        // Get the list of exercises
        getExercises();

//        String s = "";
//        for (int i = 0; i < exercisesList.size(); i++) {
//            s += exercisesList.get(i).getUserId() + ", " + exercisesList.get(i).isInclude() + ", " + exercisesList.get(i).getDescription() + ", " + exercisesList.get(i).getImageURI() + "\n";
//        }
//        Toast.makeText(this, s, Toast.LENGTH_LONG).show();

        executeWorkout(WorkoutAction.START);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Globals.wasRunning) {
            executeWorkout(WorkoutAction.RUN);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (Globals.workoutStatus.getWorkoutAction() == WorkoutAction.RUN) {
            executeWorkout(WorkoutAction.PAUSE);
            Globals.wasRunning = true;
        } else
            Globals.wasRunning = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        executeWorkout(WorkoutAction.STOP);
    }

    public void onClickStartPause(View view) {

        // User tapped on timer
        executeWorkout(WorkoutAction.TAP);
    }

    public void onClickPrevious(View view) {
        executeWorkout(WorkoutAction.PREVIOUS);
    }

    public void onClickNext(View view) {
        executeWorkout(WorkoutAction.NEXT);
    }

    public void loadSettings() {
        Settings.settings = DatabaseHelper.loadSettings(this, Globals.userId);
    }

    public void getExercises() {
        exercisesList = DatabaseHelper.getExercises(this, Globals.userId);
    }

    private void executeWorkout(WorkoutAction action) {

        if (Globals.workoutStatus == null) {
            Globals.workoutStatus = new WorkoutStatus();
            Globals.workoutStatus.setWorkoutAction(WorkoutAction.NONE);
            Globals.workoutStatus.setCurrentRound(1);
            Globals.workoutStatus.setCurrentExercise(1);
            Globals.workoutStatus.setTimerType(TimerType.NONE);
            Globals.workoutStatus.setTime(0);
        }
        String fileName;
        int resId;

        switch (action) {

            case START:
                Globals.workoutStatus.setWorkoutAction(WorkoutAction.RUN);
                Globals.workoutStatus.setCurrentRound(1);
                Globals.workoutStatus.setCurrentExercise(1);
                Globals.workoutStatus.setTimerType(TimerType.COUNTDOWN);
                Globals.workoutStatus.setTime(Settings.settings.getCountdown());
                tvCountType.setText(getResources().getString(R.string.countdown));
                tvRound.setText(getResources().getString(R.string.round) + " " + String.valueOf(Globals.workoutStatus.getCurrentRound()) + "/" + String.valueOf(Settings.settings.getRepeatCircuits()));
                tvDescription.setText(String.valueOf(Globals.workoutStatus.getCurrentExercise()) + "/" + String.valueOf(exercisesList.size()) + " " +
                        exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getDescription());
                fileName = exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getImageURI();
                fileName = fileName.substring(0, fileName.lastIndexOf("."));
                resId = getResources().getIdentifier(fileName,
                        "drawable", getPackageName());
                ivExercise.setImageResource(resId);
                runTimer();
                break;

            case TAP:
                if (Globals.workoutStatus.getWorkoutAction() == WorkoutAction.RUN) {
                    Globals.workoutStatus.setWorkoutAction(WorkoutAction.PAUSE);
                    tvCountType.setText(getResources().getString(R.string.paused));
                } else {
                    Globals.workoutStatus.setWorkoutAction(WorkoutAction.RUN);
                    if (Globals.workoutStatus.getTimerType() == TimerType.COUNTDOWN)
                        tvCountType.setText(getResources().getString(R.string.countdown));
                    else if (Globals.workoutStatus.getTimerType() == TimerType.EXERCISE)
                        tvCountType.setText(getResources().getString(R.string.do_exercise));
                    else
                        tvCountType.setText(getResources().getString(R.string.rest));
                }
                break;

            case PREVIOUS:
                if (Globals.workoutStatus.getCurrentExercise() > 1) {
                    Globals.workoutStatus.setCurrentExercise(Globals.workoutStatus.getCurrentExercise()-1);
                    Globals.workoutStatus.setTimerType(TimerType.REST);
                    Globals.workoutStatus.setTime(0);
                    tvCountType.setText(getResources().getString(R.string.rest));
                    tvDescription.setText(String.valueOf(Globals.workoutStatus.getCurrentExercise()) + "/" + String.valueOf(exercisesList.size()) + " " +
                            exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getDescription());
                    fileName = exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getImageURI();
                    fileName = fileName.substring(0, fileName.lastIndexOf("."));
                    resId = getResources().getIdentifier(fileName,
                            "drawable", getPackageName());
                    ivExercise.setImageResource(resId);
                } else if (Globals.workoutStatus.getCurrentRound() > 1) {
                    Globals.workoutStatus.setCurrentRound(Globals.workoutStatus.getCurrentRound()-1);
                    Globals.workoutStatus.setCurrentExercise(exercisesList.size());
                    Globals.workoutStatus.setTimerType(TimerType.REST);
                    Globals.workoutStatus.setTime(0);
                    tvCountType.setText(getResources().getString(R.string.rest));
                    tvRound.setText(getResources().getString(R.string.round) + " " + String.valueOf(Globals.workoutStatus.getCurrentRound()) + "/" + String.valueOf(Settings.settings.getRepeatCircuits()));
                    tvDescription.setText(String.valueOf(Globals.workoutStatus.getCurrentExercise()) + "/" + String.valueOf(exercisesList.size()) + " " +
                            exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getDescription());
                    fileName = exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getImageURI();
                    fileName = fileName.substring(0, fileName.lastIndexOf("."));
                    resId = getResources().getIdentifier(fileName,
                            "drawable", getPackageName());
                    ivExercise.setImageResource(resId);
                }

                break;
            case NEXT:
                if (Globals.workoutStatus.getCurrentExercise() < exercisesList.size()) {
                    Globals.workoutStatus.setCurrentExercise(Globals.workoutStatus.getCurrentExercise() + 1);
                    Globals.workoutStatus.setTimerType(TimerType.REST);
                    Globals.workoutStatus.setTime(0);
                    tvCountType.setText(getResources().getString(R.string.rest));
                    tvDescription.setText(String.valueOf(Globals.workoutStatus.getCurrentExercise()) + "/" + String.valueOf(exercisesList.size()) + " " +
                            exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getDescription());
                    fileName = exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getImageURI();
                    fileName = fileName.substring(0, fileName.lastIndexOf("."));
                    resId = getResources().getIdentifier(fileName,
                            "drawable", getPackageName());
                    ivExercise.setImageResource(resId);
                } else if (Globals.workoutStatus.getCurrentRound() < Settings.settings.getRepeatCircuits()) {
                    Globals.workoutStatus.setCurrentRound(Globals.workoutStatus.getCurrentRound()+1);
                    Globals.workoutStatus.setCurrentExercise(1);
                    Globals.workoutStatus.setTimerType(TimerType.REST);
                    Globals.workoutStatus.setTime(0);
                    tvCountType.setText(getResources().getString(R.string.rest));
                    tvRound.setText(getResources().getString(R.string.round) + " " + String.valueOf(Globals.workoutStatus.getCurrentRound()) + "/" + String.valueOf(Settings.settings.getRepeatCircuits()));
                    tvDescription.setText(String.valueOf(Globals.workoutStatus.getCurrentExercise()) + "/" + String.valueOf(exercisesList.size()) + " " +
                            exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getDescription());
                    fileName = exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getImageURI();
                    fileName = fileName.substring(0, fileName.lastIndexOf("."));
                    resId = getResources().getIdentifier(fileName,
                            "drawable", getPackageName());
                    ivExercise.setImageResource(resId);
                }
                break;

            case STOP:
                Globals.workoutStatus.setWorkoutAction(WorkoutAction.STOP);
                break;

            default:

        }
    }

    //Sets the number of seconds on the timer.
    private void runTimer() {
        final Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                // Call the next event immediately, otherwise execution of the code below will slow down timer
                if (Globals.workoutStatus.getWorkoutAction() != WorkoutAction.STOP) {
                    handler.postDelayed(this, 1000);
                }

                int seconds = Globals.workoutStatus.getTime();
                String time = String.format("%02d\"", seconds);
                tvTime.setText(time);
                if (Globals.workoutStatus.getWorkoutAction() == WorkoutAction.RUN) {
                    if (Globals.workoutStatus.getTimerType() == TimerType.COUNTDOWN) {
                        if (seconds > 0) {
                            Globals.workoutStatus.setTime(Globals.workoutStatus.getTime() - 1);
                            sbTimeProgress.setProgress((int)(100.0*(Settings.settings.getCountdown()-Globals.workoutStatus.getTime())/Settings.settings.getCountdown()));
                        } else {
                            playNotification();

                            // Start first exercise
                            Globals.workoutStatus.setTimerType(TimerType.EXERCISE);
                            tvCountType.setText(getResources().getString(R.string.do_exercise));
                            Globals.workoutStatus.setTime(0);
                            tvTime.setText("00");
                        }
                    }
                    if (Globals.workoutStatus.getTimerType() == TimerType.REST) {
                        if (seconds < Settings.settings.getRest()) {
                            Globals.workoutStatus.setTime(Globals.workoutStatus.getTime() + 1);
                            sbTimeProgress.setProgress((int)(100.0*Globals.workoutStatus.getTime()/Settings.settings.getRest()));
                        } else {
                            // Start next exercise
                            playNotification();
                            Globals.workoutStatus.setTimerType(TimerType.EXERCISE);
                            Globals.workoutStatus.setTime(0);
                            tvTime.setText("00");
                            tvCountType.setText(getResources().getString(R.string.do_exercise));
                            String fileName = exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getImageURI();
                            fileName = fileName.substring(0, fileName.lastIndexOf("."));
                            int resId = getResources().getIdentifier(fileName,
                                    "drawable", getPackageName());
                            ivExercise.setImageResource(resId);
                        }
                    } else if (Globals.workoutStatus.getTimerType() == TimerType.EXERCISE) {
                        if (seconds < Settings.settings.getExercisePeriod()) {
                            Globals.workoutStatus.setTime(Globals.workoutStatus.getTime() + 1);
                            sbTimeProgress.setProgress((int)(100.0*Globals.workoutStatus.getTime()/Settings.settings.getExercisePeriod()));
                        } else {
                            if (Globals.workoutStatus.getCurrentExercise() < exercisesList.size()) {
                                // Next exercise
                                playNotification();
                                Globals.workoutStatus.setCurrentExercise(Globals.workoutStatus.getCurrentExercise() + 1);
                                Globals.workoutStatus.setTimerType(TimerType.REST);
                                Globals.workoutStatus.setTime(0);
                                tvTime.setText("00");
                                tvCountType.setText(getResources().getString(R.string.rest));
                                tvDescription.setText(String.valueOf(Globals.workoutStatus.getCurrentExercise()) + "/" + String.valueOf(exercisesList.size()) + " " +
                                        exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getDescription());
                                String fileName = exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getImageURI();
                                fileName = fileName.substring(0, fileName.lastIndexOf("."));
                                int resId = getResources().getIdentifier(fileName,
                                        "drawable", getPackageName());
                                ivExercise.setImageResource(resId);
                            } else if (Globals.workoutStatus.getCurrentRound() < Settings.settings.getRepeatCircuits()) {
                                // Next round
                                playNotification();
                                Globals.workoutStatus.setCurrentRound(Globals.workoutStatus.getCurrentRound() + 1);
                                Globals.workoutStatus.setCurrentExercise(1);
                                Globals.workoutStatus.setTimerType(TimerType.REST);
                                Globals.workoutStatus.setTime(0);
                                tvTime.setText("00");
                                tvCountType.setText(getResources().getString(R.string.rest));
                                tvRound.setText(getResources().getString(R.string.round) + " " + String.valueOf(Globals.workoutStatus.getCurrentRound()) + "/" + String.valueOf(Settings.settings.getRepeatCircuits()));
                                tvDescription.setText(String.valueOf(Globals.workoutStatus.getCurrentExercise()) + "/" + String.valueOf(exercisesList.size()) + " " +
                                        exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getDescription());
                                String fileName = exercisesList.get(Globals.workoutStatus.getCurrentExercise()-1).getImageURI();
                                fileName = fileName.substring(0, fileName.lastIndexOf("."));
                                int resId = getResources().getIdentifier(fileName,
                                        "drawable", getPackageName());
                                ivExercise.setImageResource(resId);
                            } else {
                                // All exercises are done, but don't stop the timer...
                                // Maybe user wants to repeat previous exercises
                                Globals.workoutStatus.setWorkoutAction(WorkoutAction.PAUSE);
                            }
                        }
                    }
                }

            }
        });
    }

    private void playNotification() {
        try {
            if (Settings.settings.isSound()) {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
                r.play();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}