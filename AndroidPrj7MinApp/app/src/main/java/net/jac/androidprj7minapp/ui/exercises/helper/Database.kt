package net.jac.androidprj7minapp.ui.exercises.helper

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteException
import android.util.Log
import android.widget.Toast
import net.jac.androidprj7minapp.DatabaseHelper
import net.jac.androidprj7minapp.Globals
import net.jac.androidprj7minapp.ui.exercises.Exercise


//"CREATE TABLE exercises(                        " +
//"     id INTEGER PRIMARY KEY AUTOINCREMENT,     " +
//"     userId INTEGER,                           " +
//"     description TEXT,                         " +
//"     exerciseOrder INTEGER,                    " +
//"     include INTEGER,                          " +
//"     imageURI TEXT,                            " +
//"     videoURI TEXT,                            " +
//"     FOREIGN KEY(userId) REFERENCES users(id));"


fun saveExercises(context: Context, exercises: List<Exercise>, isNew: Boolean = false): Int {
    var newId = -1

    val databaseHelper = DatabaseHelper(context)
    try {
        databaseHelper.writableDatabase.use { db ->
            exercises.forEachIndexed{idx,exercise ->
                if (!isNew)
//                    db.query(
//                    "exercises", arrayOf("id"),
//                    "description = ?", arrayOf(exercise.name), null, null, null)
//                .use { cursor ->
//                    //Move to the first record in the Cursor
//                    if (cursor.moveToFirst())
                    {
                        // The exercise already exists;
                        ContentValues().apply {
                            put("exerciseOrder",idx)
                            put("include",exercise.isActive)
                            db.update("exercises", this,
                                "id = ?", arrayOf(exercise.id.toString()))
                        }
                        return@forEachIndexed
                    }
//                }

                // The exercise doesn't exist - add new exercise

                ContentValues().apply {
                    put("description", exercise.name)
                    put("imageURI", exercise.imgName)
                    put("include", exercise.isActive)
                    put("exerciseOrder", exercise.id)
                    put("userId", Globals.userId)
                    newId = db.insert("exercises", null, this).toInt()
                }
                Log.d(TAG, "addExercise -> Successfully added exercise " + exercise.name + " (Id = " + newId.toString() + ")")
            }
        }

    } catch (e: Throwable) {//SQLiteException
        // FIXME: If there's an exception - we should handle the problem. Possible fixes are below...
        // 1) We could "rollback" the whole transaction...
        // 2) We can make sure that user can setup the settings manually...
        val toast = Toast.makeText(context, "Database unavailable", Toast.LENGTH_SHORT)
        toast.show()
    }

    return newId
}

fun getExercises(context: Context, exercises: ArrayList<Exercise>): Int {
    var inserts = 0
    val databaseHelper = DatabaseHelper(context)
    try {
        databaseHelper.writableDatabase.use { db ->
            db.query(
                "exercises", arrayOf("id","description","include","imageURI"),
                "userId = ?", arrayOf(Globals.userId.toString()), null, null, "exerciseOrder asc"
            ).use { cursor ->
                cursor.apply {
                    while (moveToNext()) {
                        exercises += Exercise(
                            id = getInt(0),
                            name = getString(1),
                            imgName = getString(3),
                            isActive = getInt(2) == 1
                        )
                        inserts++
                    }
                }
            }
        }
    } catch (e:SQLiteException) {
        Toast.makeText(context, "Database unavailable", Toast.LENGTH_SHORT).show()
    }

    return inserts
}


    const val TAG: String="Database"
