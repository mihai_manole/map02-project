package net.jac.androidprj7minapp.ui.exercises

import android.arch.lifecycle.ViewModel
import android.view.View

class ExercisesViewModel : ViewModel() {
    val exercisesList= Exercise.exercisesList
}

class Exercise(
    val name: String,
    val imgName: String,
    val id: Int = lastOrder,
    var isActive: Boolean = true,
    var isDone: Boolean = false
){

//    constructor(pair: Pair<String,String>):this(pair.first,pair.second)
    companion object {
        private var lastOrder = 0
        get() = field++

        val exercisesList= arrayListOf<Exercise>()
        var isChanged = false
        fun markAsChanged(view: View?=null) {isChanged = true}
        fun setList(descriptions: Array<String>, imagesName: Array<String>){
            if (exercisesList.isNotEmpty()) return
            exercisesList.addAll(descriptions.asSequence().zip(imagesName.asSequence()).map {(name,imgName) -> Exercise(name,imgName) })
        }
    }
}
