package net.jac.androidprj7minapp

data class Instruction(
    var name: String,
    var img: String,
    var instruction: String
)
