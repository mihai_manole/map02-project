package net.jac.androidprj7minapp;

import java.util.Date;

public class Settings {

    // settings instance is created in MainActivity in onCreate and used whenever settings are needed.
    public static Settings settings;
    enum SettingsProperty {
        DATE_OF_BIRTH, HEIGHT, REPEAT_CIRCUITS, EXERCISE_PERIOD, REST, COUNTDOWN, SOUND, VOICE, METRIC
    }

        // userId dateOfBirth height repeatCircuits exercisePeriod rest countdown sound voice metric
    private int userId;
    private Date dateOfBirth;
    private double height;
    private int repeatCircuits;
    private int exercisePeriod;
    private int rest;
    private int countdown;
    private boolean sound;
    private boolean voice;
    private boolean metric;

    public Settings() {
    }

    public Settings(int userId, Date dateOfBirth, double height, int repeatCircuits, int exercisePeriod, int rest, int countdown, boolean sound, boolean voice, boolean metric) {
        this.userId = userId;
        this.dateOfBirth = dateOfBirth;
        this.height = height;
        this.repeatCircuits = repeatCircuits;
        this.exercisePeriod = exercisePeriod;
        this.rest = rest;
        this.countdown = countdown;
        this.sound = sound;
        this.voice = voice;
        this.metric = metric;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public int getRepeatCircuits() {
        return repeatCircuits;
    }

    public void setRepeatCircuits(int repeatCircuits) {
        this.repeatCircuits = repeatCircuits;
    }

    public int getExercisePeriod() {
        return exercisePeriod;
    }

    public void setExercisePeriod(int exercisePeriod) {
        this.exercisePeriod = exercisePeriod;
    }

    public int getRest() {
        return rest;
    }

    public void setRest(int rest) {
        this.rest = rest;
    }

    public int getCountdown() {
        return countdown;
    }

    public void setCountdown(int countdown) {
        this.countdown = countdown;
    }

    public boolean isSound() {
        return sound;
    }

    public void setSound(boolean sound) {
        this.sound = sound;
    }

    public boolean isVoice() {
        return voice;
    }

    public void setVoice(boolean voice) {
        this.voice = voice;
    }

    public boolean isMetric() {
        return metric;
    }

    public void setMetric(boolean metric) {
        this.metric = metric;
    }
}
