package net.jac.androidprj7minapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.lang.reflect.Array;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String TAG = "DatabaseHelper";
    private static final String DB_NAME = "projectdb"; // the name of our database
    private static final int DB_VERSION = 1; // the version of the database

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db, 0, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db, oldVersion, newVersion);
    }

    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 1) {
            // Create users table
            db.execSQL(
                "CREATE TABLE users(                         " +
                "     id INTEGER PRIMARY KEY AUTOINCREMENT,  " +
                "     name TEXT);                            "
            );
            // Create settings table
            db.execSQL(
                "CREATE TABLE settings(                         " +
                "     userId INTEGER,                           " +
                "     dateOfBirth TEXT,                         " +
                "     height NUMERIC,                           " +
                "     repeatCircuits INTEGER,                   " +
                "     exercisePeriod INTEGER,                   " +
                "     rest INTEGER,                             " +
                "     countdown INTEGER,                        " +
                "     sound INTEGER,                            " +
                "     voice INTEGER,                            " +
                "     metric INTEGER,                           " +
                "     FOREIGN KEY(userId) REFERENCES users(id));"
            );
            // Create workouts table
            db.execSQL(
                "CREATE TABLE workouts(                         " +
                "     userId INTEGER,                           " +
                "     startDateTime TEXT,                       " +
                "     endDateTime TEXT,                         " +
                "     exercisesList TEXT,                       " +
                "     FOREIGN KEY(userId) REFERENCES users(id));"
            );
            // Create exercises table
            db.execSQL(
                "CREATE TABLE exercises(                        " +
                "     id INTEGER PRIMARY KEY AUTOINCREMENT,     " +
                "     userId INTEGER,                           " +
                "     description TEXT,                         " +
                "     exerciseOrder INTEGER,                    " +
                "     include INTEGER,                          " +
                "     imageURI TEXT,                            " +
                "     videoURI TEXT,                            " +
                "     FOREIGN KEY(userId) REFERENCES users(id));"
            );
            // Create weights table
            db.execSQL(
                "CREATE TABLE weights(                          " +
                "     userId INTEGER,                           " +
                "     weighingDateTime TEXT,                    " +
                "     weight NUMERIC,                           " +
                "     FOREIGN KEY(userId) REFERENCES users(id));"
            );
            // Create reminders table
            db.execSQL(
                "CREATE TABLE reminders(                          " +
                "     userId INTEGER,                           " +
                "     time TEXT,                                " +
                "     days TEXT,                                " +
                "     FOREIGN KEY(userId) REFERENCES users(id));"
            );
            Log.d(TAG, "updateMyDatabase -> All tables created successfully");

        }
        if (oldVersion < 2) {
            //db.execSQL("ALTER TABLE TABLE_NAME ADD COLUMN COLUMN_NAME NUMERIC;");
        }
    }

    public static int addUser(Context context, String name) {
        int newId = 0;

        SQLiteOpenHelper databaseHelper = new DatabaseHelper(context);
        try {
            SQLiteDatabase db = databaseHelper.getWritableDatabase();

            // Check if name already exists
            // NOTE: The query is case sensitive, "John" and "john" are different users
            //Create a cursor
            Cursor cursor = db.query("users",
                    new String[]{"name"},
                    "name = ?",
                    new String[]{name},
                    null, null, null);
            //Move to the first record in the Cursor
            if (cursor.moveToFirst()) {
                // User name already exists;
                Log.d(TAG, "addUser -> User \"" + name + "\" already exists");
                db.close();
                return -1;
            }

            // Name doesn't exist - add new user and setup default settings

            // Add user
            ContentValues userValues = new ContentValues();
            userValues.put("name", name);
            newId = (int)db.insert("users", null, userValues);
            Log.d(TAG, "addUser -> Successfully added user " + name + " (Id = " + String.valueOf(newId) + ")");
            // Add settings for the new user (Use hardcoded default values)
            ContentValues settingsValues = new ContentValues();
            settingsValues.put("userId", newId);
            settingsValues.put("repeatCircuits", 1);
            settingsValues.put("exercisePeriod", 30);
            settingsValues.put("rest", 10);
            settingsValues.put("countdown", 15);
            settingsValues.put("sound", 1);
            settingsValues.put("voice", 1);
            settingsValues.put("metric", 1);
            db.insert("settings", null, settingsValues);

            db.close();

            Log.d(TAG, "addUser -> Successfully updated default settings for new user (Name " + name + ", Id = " + String.valueOf(newId) + ")");

        } catch (SQLiteException e) {
            // FIXME: If there's an exception - we should handle the problem. Possible fixes are below...
            // 1) We could "rollback" the whole transaction...
            // 2) We can make sure that user can setup the settings manually...
            Toast toast = Toast.makeText(context, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }
        return newId;
    }

    public static Settings loadSettings(Context context, int id) {

        Settings settings = new Settings();

        //Create a cursor
        SQLiteOpenHelper databaseHelper = new DatabaseHelper(context);
        try {
            SQLiteDatabase db = databaseHelper.getReadableDatabase();
            Cursor cursor = db.query("settings",
                    new String[]{"userId", "dateOfBirth", "height", "repeatCircuits", "exercisePeriod", "rest", "countdown", "sound", "voice", "metric"},
                    "userId = ?",
                    new String[]{Integer.toString(id)},
                    null, null, null);
            //Move to the first record in the Cursor
            if (cursor.moveToFirst()) {

                settings.setUserId(cursor.getInt(0));
                //SQLite stores DateTime in "YYYY-MM-DD HH:MM:SS.SSS" format
                String pattern = "YYYY-MM-DD HH:MM:SS.SSS";
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
                try {
                    if (cursor.getString(1) != null)
                        settings.setDateOfBirth(simpleDateFormat.parse(cursor.getString(1)));
                } catch (ParseException ex){
                    Log.e(TAG, "Error parsing settings.dateOfBirth field", ex);
                }
                settings.setHeight(cursor.getDouble(2));
                settings.setRepeatCircuits(cursor.getInt(3));
                settings.setExercisePeriod(cursor.getInt(4));
                settings.setRest(cursor.getInt(5));
                settings.setCountdown(cursor.getInt(6));
                settings.setSound(cursor.getInt(7) == 1);
                settings.setVoice(cursor.getInt(8) == 1);
                settings.setMetric(cursor.getInt(9) == 1);

            }

            db.close();

        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(context, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        return settings;
    }

    public static ArrayList<String> getUserNames(Context context, ArrayList<String> userNames) {

        //Create a cursor
        SQLiteOpenHelper databaseHelper = new DatabaseHelper(context);
        try {
            SQLiteDatabase db = databaseHelper.getReadableDatabase();
            Cursor cursor = db.query("users", new String[]{"name"},
                    null, null,
                    null, null, null);
            // Move to first row and move next until end of cursor
            if (cursor.moveToFirst()) {
                userNames.add(cursor.getString(0));
                while (cursor.moveToNext()) {
                    userNames.add(cursor.getString(0));
                }
            }

            db.close();

        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(context, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        return userNames;
    }


    public static ArrayList<Exercise> getExercises(Context context, int id) {
        ArrayList<Exercise> exercises = new ArrayList<>();

        //Create a cursor
        SQLiteOpenHelper databaseHelper = new DatabaseHelper(context);
        try {
            SQLiteDatabase db = databaseHelper.getReadableDatabase();
            Cursor cursor = db.query("exercises",
                    new String[]{"userId", "description", "include", "imageURI", "videoURI"},
                    "userId = ? AND include = 1",
                    new String[]{Integer.toString(id)},
                    null, null, "exerciseOrder");

            //Move to the first record in the Cursor
            if (cursor.moveToFirst()) {

                int userId = cursor.getInt(0);
                String description = cursor.getString(1);
                boolean include = cursor.getInt(2) == 1;
                String imageURI = cursor.getString(3);
                String videoURI = cursor.getString(4);
                Exercise exercise = new Exercise(userId, description, include, imageURI, videoURI);
                exercises.add(exercise);

                while (cursor.moveToNext()) {
                    userId = cursor.getInt(0);
                    description = cursor.getString(1);
                    include = cursor.getInt(2) == 1;
                    imageURI = cursor.getString(3);
                    videoURI = cursor.getString(4);
                    exercise = new Exercise(userId, description, include, imageURI, videoURI);
                    exercises.add(exercise);
                }
            }

            db.close();

        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(context, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        return exercises;
    }

    public static int getUserIdByName(Context context, String name) {

        int result = 0;
        //Create a cursor
        SQLiteOpenHelper databaseHelper = new DatabaseHelper(context);
        try {
            SQLiteDatabase db = databaseHelper.getReadableDatabase();
            Cursor cursor = db.query("users", new String[]{"id"},
                    "name = ?", new String[]{name},
                    null, null, null);
            //Move to the first record in the Cursor
            if (cursor.moveToFirst()) {

                result = cursor.getInt(0);
            } else {
                result = -1;
            }

            db.close();

        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(context, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

        return result;
    }

    public static void updateSetting(Context context, Settings.SettingsProperty settingsProperty, int newValue) {

        ContentValues settingValues = new ContentValues();

        SQLiteOpenHelper databaseHelper = new DatabaseHelper(context);

        try {
            SQLiteDatabase db = databaseHelper.getWritableDatabase();

            switch (settingsProperty) {

                case REPEAT_CIRCUITS:
                    settingValues.put("repeatCircuits", newValue);
                    break;
                case EXERCISE_PERIOD:
                    settingValues.put("exercisePeriod", newValue);
                    break;
                case REST:
                    settingValues.put("rest", newValue);
                    break;
                case COUNTDOWN:
                    settingValues.put("countdown", newValue);
                    break;
                case SOUND:
                    settingValues.put("sound", newValue);
                    break;
                case VOICE:
                    settingValues.put("voice", newValue);
                    break;

                default :
                    Log.d(TAG, "updateSetting -> Invalid argument: " + settingsProperty.toString());

            }

            db.update("settings", settingValues,
                    "userId = ?", new String[]{Integer.toString(Globals.userId)});
            db.close();

            Log.d(TAG, "updateSetting -> Successfully updated setting " + settingsProperty.toString());

        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(context, "Database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }

    }
}
