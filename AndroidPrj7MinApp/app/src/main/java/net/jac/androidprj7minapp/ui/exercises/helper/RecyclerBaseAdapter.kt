package net.jac.androidprj7minapp.ui.exercises.helper

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import net.jac.androidprj7minapp.BR

abstract class RecyclerBaseAdapter<VH:RecyclerViewHolder>(private val layoutId: Int) : RecyclerView.Adapter<VH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        getVH(DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(parent.context), viewType, parent, false))

    override fun onBindViewHolder(holder: VH, position: Int) {
        getViewModel(position)
            ?.let {
                val bindingSuccess = holder.binding.setVariable(BR.viewModel, it)
                if (!bindingSuccess) {
                    throw IllegalStateException("Binding ${holder.binding} viewModel variable name should be 'viewModel'")
                }
            }
    }

    override fun getItemViewType(position: Int) = layoutId

    abstract fun getViewModel(position: Int): Any?

    abstract fun getVH(binding: ViewDataBinding): VH
}


open class RecyclerViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root)
