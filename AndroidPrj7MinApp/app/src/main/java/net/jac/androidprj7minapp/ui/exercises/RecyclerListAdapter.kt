package net.jac.androidprj7minapp.ui.exercises

import android.databinding.ViewDataBinding
import android.support.v7.widget.CardView
import net.jac.androidprj7minapp.R
import net.jac.androidprj7minapp.ui.exercises.helper.ItemTouchHelperAdapter
import net.jac.androidprj7minapp.ui.exercises.helper.ItemTouchHelperViewHolder
import net.jac.androidprj7minapp.ui.exercises.helper.RecyclerBaseAdapter
import net.jac.androidprj7minapp.ui.exercises.helper.RecyclerViewHolder
import java.util.*
import kotlin.collections.ArrayList


class RecyclerListAdapter(private val exercisesList: ArrayList<Exercise>
) : RecyclerBaseAdapter<RecyclerListAdapter.ItemViewHolder>(R.layout.item_exercises), ItemTouchHelperAdapter {
    override fun getVH(binding: ViewDataBinding): ItemViewHolder = ItemViewHolder(binding)

    override fun getViewModel(position: Int): Any = this.exercisesList[position]

    override fun onItemDismiss(position: Int) {
        this.exercisesList.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int) {
        Collections.swap(this.exercisesList, fromPosition, toPosition)
        Exercise.markAsChanged()
        notifyItemMoved(fromPosition, toPosition)
    }

    override fun getItemCount(): Int {
        return this.exercisesList.size
    }

    class ItemViewHolder(binding: ViewDataBinding) : RecyclerViewHolder(binding), ItemTouchHelperViewHolder {

//        val itemView = binding.root
    var initCardElevation = 0f

        override fun onItemSelected() {
//            itemView.setBackgroundColor(Color.LTGRAY)
            if (itemView is CardView) {
                initCardElevation=itemView.cardElevation
                itemView.cardElevation=initCardElevation*.5f
//                itemView.ma
            }
        }

        override fun onItemClear() {
//            itemView.setBackgroundColor(0)
            if (itemView is CardView) {
                itemView.cardElevation=initCardElevation
            }
        }
    }

    companion object {

        private val STRINGS = arrayOf("One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten")
    }
}
