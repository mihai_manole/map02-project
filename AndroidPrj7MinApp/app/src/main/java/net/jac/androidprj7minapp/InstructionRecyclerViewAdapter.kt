package net.jac.androidprj7minapp

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import kotlinx.android.synthetic.main.fragment_instruction.view.*
import net.jac.androidprj7minapp.R.id.ivExercise

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class InstructionRecyclerViewAdapter(
    private val mValues: List<Instruction>
) : RecyclerView.Adapter<InstructionRecyclerViewAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_instruction, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.nameView.text = item.name
        holder.instructionView.text = item.instruction

        val fileName = item.img.run { substring(0, lastIndexOf(".")) }
        val resId = holder.mView.context.run {
            resources.getIdentifier(fileName,"drawable", packageName
        )}
        holder.imageView.setImageResource(resId)

//        holder.mView.tag = item

    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val nameView: TextView = mView.tvExName
        val instructionView: TextView = mView.tvExInstruction
        val imageView: ImageView = mView.tvExImage

        override fun toString(): String {
            return super.toString() + " '" + instructionView.text + "'"
        }
    }
}
