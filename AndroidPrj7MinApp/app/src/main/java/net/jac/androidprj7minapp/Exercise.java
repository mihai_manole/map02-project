package net.jac.androidprj7minapp;

public class Exercise {
    private int userId;
    private String description;
    private boolean include;
    private String imageURI;
    private String videoURI;

    public Exercise(int userId, String description, boolean include, String imageURI, String videoURI) {
        this.userId = userId;
        this.description = description;
        this.include = include;
        this.imageURI = imageURI;
        this.videoURI = videoURI;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isInclude() {
        return include;
    }

    public void setInclude(boolean include) {
        this.include = include;
    }

    public String getImageURI() {
        return imageURI;
    }

    public void setImageURI(String imageURI) {
        this.imageURI = imageURI;
    }

    public String getVideoURI() {
        return videoURI;
    }

    public void setVideoURI(String videoURI) {
        this.videoURI = videoURI;
    }
}
