package net.jac.androidprj7minapp.ui.exercises

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.exercises_fragment.*
import net.jac.androidprj7minapp.ui.exercises.helper.SimpleItemTouchHelperCallback
import net.jac.androidprj7minapp.R
import net.jac.androidprj7minapp.WorkoutActivity
import net.jac.androidprj7minapp.ui.exercises.helper.getExercises
import net.jac.androidprj7minapp.ui.exercises.helper.saveExercises

class ExercisesFragment : Fragment() {

    companion object {
        fun newInstance() = ExercisesFragment()
    }

    private lateinit var mItemTouchHelper: ItemTouchHelper
    private lateinit var viewModel: ExercisesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.exercises_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProviders.of(this).get(ExercisesViewModel::class.java)
        // TODO: Use the ViewModel
        val context=context ?: return
        Exercise.isChanged=false
        if (viewModel.exercisesList.isEmpty()) with(viewModel) {
            val count=getExercises(context,exercisesList)
            if (count==0) {
                Exercise.setList(
                    context.resources.getStringArray(R.array.classic_exercises),
                    context.resources.getStringArray(R.array.img_classic_exercises))
                saveExercises(context, exercisesList, true)
                exercisesList.clear()
                getExercises(context, exercisesList)
            }
        }

        val adapter = RecyclerListAdapter(viewModel.exercisesList)

        val recyclerView = recycler_view//view.findViewById(R.id.recycler_view) as RecyclerView
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity)

        val callback = SimpleItemTouchHelperCallback(adapter)
        mItemTouchHelper = ItemTouchHelper(callback)
        mItemTouchHelper.attachToRecyclerView(recyclerView)

        // Open WorkoutActivity when user clicks "Go" label
        clGo.setOnClickListener {
            startActivity(Intent(context, WorkoutActivity::class.java))
            Exercise.run {
                if (isChanged) {
                    saveExercises(context, exercisesList, false)
                    Toast.makeText(context, "Saved", Toast.LENGTH_SHORT).show()
                }
                isChanged = false
            }
        }

    }

}
