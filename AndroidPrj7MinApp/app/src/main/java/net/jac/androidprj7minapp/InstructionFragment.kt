package net.jac.androidprj7minapp

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [InstructionFragment.OnListFragmentInteractionListener] interface.
 */
class InstructionFragment : Fragment() {

    // TODO: Customize parameters
    private var columnCount = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_instruction_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            with(view) {
                layoutManager = when {
                    columnCount <= 1 -> LinearLayoutManager(context)
                    else -> GridLayoutManager(context, columnCount)
                }

                val data=inflater.context.resources.run {
                    getStringArray(R.array.classic_exercises).asSequence().zip(
                        getStringArray(R.array.img_classic_exercises).asSequence()).zip(
                        getStringArray(R.array.instructions_classic_exercises).asSequence()){
                            (a, b),c -> Instruction(a,b,c)
                    }.toList()
                }
                adapter = InstructionRecyclerViewAdapter(data)
//
//                val adapter = RecyclerListAdapter(DummyContent.ITEMS)
//
//                val recyclerView = this@with
//                recyclerView.setHasFixedSize(true)
//                recyclerView.adapter = adapter
//                recyclerView.layoutManager = LinearLayoutManager(activity)
//
//                val callback = SimpleItemTouchHelperCallback(adapter)
//                mItemTouchHelper = ItemTouchHelper(callback)
//                mItemTouchHelper.attachToRecyclerView(recyclerView)
            }
        }
        return view
    }



    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            InstructionFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}
