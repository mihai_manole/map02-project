package net.jac.androidprj7minapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import net.jac.androidprj7minapp.ui.exercises.Exercise
import net.jac.androidprj7minapp.ui.exercises.ExercisesFragment
import net.jac.androidprj7minapp.ui.exercises.helper.saveExercises

class ExercisesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.exercises_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, ExercisesFragment.newInstance())
                .commitNow()
        }
    }

    override fun onDestroy() {
        Exercise.run {
            if (isChanged) {
                saveExercises(this@ExercisesActivity, exercisesList, false)
                Toast.makeText(this@ExercisesActivity, "Saved", Toast.LENGTH_SHORT).show()
            }
            exercisesList.clear()
        }
        super.onDestroy()
    }
}
