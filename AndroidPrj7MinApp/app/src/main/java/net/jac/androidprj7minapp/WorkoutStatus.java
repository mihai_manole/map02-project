package net.jac.androidprj7minapp;

import net.jac.androidprj7minapp.WorkoutActivity;

public class WorkoutStatus {
    private WorkoutActivity.WorkoutAction workoutAction;
    private int currentRound;
    private int currentExercise;
    private WorkoutActivity.TimerType timerType;
    private int time;


    public WorkoutStatus() {

    }

    public WorkoutActivity.WorkoutAction getWorkoutAction() {
        return workoutAction;
    }

    public void setWorkoutAction(WorkoutActivity.WorkoutAction workoutAction) {
        this.workoutAction = workoutAction;
    }

    public int getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(int currentRound) {
        this.currentRound = currentRound;
    }

    public int getCurrentExercise() {
        return currentExercise;
    }

    public void setCurrentExercise(int currentExercise) {
        this.currentExercise = currentExercise;
    }

    public WorkoutActivity.TimerType getTimerType() {
        return timerType;
    }

    public void setTimerType(WorkoutActivity.TimerType timerType) {
        this.timerType = timerType;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

}
